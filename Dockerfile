# Set the base image for subsequent instructions
FROM php:7.2

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev \ 
    libfreetype6-dev libbz2-dev jq locales nodejs \
    && sed -i 's/# en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/g' /etc/locale.gen \
    && sed -i 's/# nl_NL.UTF-8 UTF-8/nl_NL.UTF-8 UTF-8/g' /etc/locale.gen \
    && rm -rf /etc/locale.alias /usr/share/locale/locale.alias \
    && ln -s /etc/locale.alias /usr/share/locale/locale.alias \
    && locale-gen nl_NL.UTF-8

ENV LANG nl_NL.UTF-8
ENV LANGUAGE nl_NL:nl
ENV LC_ALL nl_NL.UTF-8

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need 
# during the test and deployment process
RUN docker-php-ext-install pdo_mysql zip gd pcntl bcmath




# Enable and configure xdebug
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug





# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer \ 
    | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"